<?php
/*
Plugin Name: Meet the Team
Plugin URI: http://www.roadsidemultimedia.com
Description: Meet The Team widget with popup. 
Author: Curtis Grant
PageLines: true
Version: 1.0.6
Section: true
Class Name: MeetTheTeam
Filter: component
Text Domain: dms-sms
Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-meet-the-team
Bitbucket Branch: master
*/
if( ! class_exists( 'PageLinesSectionFactory' ) )
  return;


class MeetTheTeam extends PageLinesSection {

  function section_styles(){
    wp_enqueue_style( 'MeetTheTeam', $this->base_url.'/css/MeetTheTeam.css');
    wp_enqueue_script( 'MeetTheTeam-js', $this->base_url.'/js/mtt.js', array( 'jquery' ), pl_get_cache_key(), true );
  }

  function section_opts(){

    global $sms_utils;
    $sms_options = $sms_utils->sms_options;


    $options = array();
    
    $options[] =
      array(
        'type'  => 'multi',
        'col'   => 1,
        'title' => 'Meet The Team Options',
        'opts'  => array(
          array(
            'type'      => 'select',
            'key'     => 'mtt_hdr_font',
            'label'     => 'Font Type HDR',
            'opts'      => $font_type_list,
          ),
          array(
            'type'      => 'select',
            'key'     => 'mtt_hdr_size',
            'label'     => 'Team Member Font Size',
            'opts'      => $size_name_list,
          ),
          array(
            'type'      => 'select',
            'key'     => 'mtt_subhdr_size',
            'label'     => 'Team Member Position Font Size',
            'opts'      => $size_name_list,
          ),

          array(
            'type'      => 'color',
            'key'     => 'mtt_border_color',
            'label'     => 'Border Color',
          ),
                   
          )
);
$options[] =
      array(
        'key'   => 'BioPopup',
        'type'    => 'multi', 
        'col'   => 2,
        'title' => 'Team Member Settings',
        'opts'  => array(
          array(
          'key'   => 'mtt_image',
          'label'   => __( 'Team Member Image', 'pagelines' ),
          'type'    => 'image_upload',
        ), 
          
                    array(
              'key'   => 'mtt_hdr',
              'label'   => __( 'Team Member Name', 'pagelines' ),
              'type'    => 'text',
          ),
                    array(
              'key'   => 'mtt_subhdr',
              'label'   => __( 'Team Member Position', 'pagelines' ),
              'type'    => 'text',
          ),        
                    array(
              'key'   => 'mtt_text',
              'label'   => __( 'Bio Text', 'pagelines' ),
              'type'    => 'textarea',
          ),
        ),
      );

    
    return $options;

  }

   function section_template( ) { 
    
    $mtthdrfont = ( $this->opt('mtt_hdr_font') ) ? $this->opt('mtt_hdr_font') : "";
    $mtthdrsize = ( $this->opt('mtt_hdr_size') ) ? $this->opt('mtt_hdr_size') : "";
    $mttsubhdrsize = ( $this->opt('mtt_subhdr_size') ) ? $this->opt('mtt_subhdr_size') : "";
    $mttbordercolor = ( $this->opt('mtt_border_color') ) ? $this->opt('mtt_border_color') : "";
    $mttimage = ( $this->opt('mtt_image') ) ? $this->opt('mtt_image') : "";
    $mtttext = ( $this->opt('mtt_text') ) ? $this->opt('mtt_text') : "";
    $mtttext = wpautop($mtttext);
    $mtthdr = ( $this->opt('mtt_hdr') ) ? $this->opt('mtt_hdr') : "";
    $mttsubhdr = ( $this->opt('mtt_subhdr') ) ? $this->opt('mtt_subhdr') : "";
    $mtthdrclass = str_replace(' ', '_', $mtthdr);
      
   ?>
<div class="textbox-wrap pl-animation animation-loaded mtt ">
  <div class="staff" data-sync="textbox_content">
      <a href="#<?php echo $mtthdrclass; ?>popup" class="mttpopup">
        <img src="<?php echo $mttimage; ?>">
      </a>
  </div>
  <h2 class="<?php echo $mtthdrsize; ?> <?php echo $mtthdrfont; ?>"><?php echo $mtthdr; ?></h2>
  <h3 class="<?php echo $mttsubhdrsize; ?> <?php echo $mtthdrfont; ?>"><?php echo $mttsubhdr; ?></h3>
</div>
<div class="bio-popup mfp-hide" id="<?php echo $mtthdrclass; ?>popup">
  <div class="bio-popup-text">
    <div class="photo">
    <img src="<?php echo $mttimage; ?>">
  </div>
    <?php echo $mtttext; ?>
  </div>
</div>

  

<?php 

}


}